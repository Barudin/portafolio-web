userList = [];

let user = {
    email: "",
    password: "",
    username: "",
    name: "",
    first_last_name: "",
    second_last_name: "",
    birthday: "",
    cell_phone: ""
}

window.onload = function(){
    var usuarioPrueba = new Object();
    usuarioPrueba.email = "alfonsog.badilla@gmail.com";
    usuarioPrueba.password = "Alfonso12";
    usuarioPrueba.username = "Barudin";
    usuarioPrueba.name = "Alfonso";
    usuarioPrueba.first_last_name = "Badilla";
    usuarioPrueba.second_last_name = "Villegas";
    usuarioPrueba.birthday = "01/10/1996";
    usuarioPrueba.cell_phone = "83185830";
    userList.push(usuarioPrueba);
}

function btnForgot(){
    document.location.href = "forgotPassword.html";
}

function btnChange() {
    document.location.href = "modifyPassword.html";
}

function createUser() {
    var name = document.getElementById("idName");
    var fLastName = document.getElementById("idFLastName");
    var sLastName = document.getElementById("idSLastName");
    var email = document.getElementById("idEmail");
    var passwordU = document.getElementById("idPassword");
    var username = document.getElementById("idUsername");
    var birthday = document.getElementById("idBirthday");
    var cellPhone = document.getElementById("idNumCell");
    var newUser = new Object();
    newUser.name = name.value;
    newUser.first_last_name = fLastName.value;
    console.log(newUser);
    newUser.second_last_name = sLastName.value;
    newUser.email = email.value;
    newUser.password = passwordU.value;
    newUser.username = username.value;
    newUser.birthday = birthday.value;
    newUser.cell_phone = cellPhone.value;

    userList.push(newUser);
}

//Log in functions

function validatesLogin() {
    var loginEmail = document.getElementById("loginEmail");
    var loginPassword = document.getElementById("loginPassword");
    if(validateEmail(loginEmail.value) == true){
        document.getElementById("status").innerHTML = ""
        startSession(loginEmail,loginPassword);
    }
    else{
        document.getElementById("status").innerHTML = "Invalid Email Address"
        //alert('Invalid Email Address');
    }
}

function startSession(loginEmail, loginPassword) {
    for (i = 0; i != userList.length; i++){
        if(userList[i].email == loginEmail.value) {
            if(userList[i].password == loginPassword.value){
                var nameUser = userList[i].name;
                var fLastName = userList[i].first_last_name;
                localStorage.setItem("nameClient", nameUser);
                localStorage.setItem("lastName", fLastName);
                localStorage.setItem("email", loginEmail.value);
                document.location.href = "logged.html";
            }
            else{
                document.getElementById("status").innerHTML = "Incorrect password/email"
                return false;
            }
        }
    }
    document.getElementById("status").innerHTML = "Unregistered user"
    return false;
}

//Modify password functions
function validatesModify() {
    var email = document.getElementById("idMEmail");
    var password = document.getElementById("idMPassword");
    var npassword = document.getElementById("idMNewPassword");
    var cpassword = document.getElementById("idMCPassword");

    if (validateEmail(email.value) == true){
        if(validatesPassword(npassword.value) == true){
            if(npassword.value == cpassword.value){
                validateCorrectPassword(email.value, password.value);
                return true;
            }
            document.getElementById("status").innerHTML = "Passwords are different";
            return false;
        }
        document.getElementById("status").innerHTML = "Invalid password";
        return false;
    }
    document.getElementById("status").innerHTML = "Invalid email";
    return false;
}

function validateCorrectPassword(email, password) {
    for (i = 0; i != userList.length; i++){
        if (userList[i].email == email && userList[i].password == password){
            document.getElementById("status").innerHTML = "Password has been changed";
            return true;
        }
    }
    document.getElementById("status").innerHTML = "Incorrect password";
    return false
}

//Sign in functions
function validateSignIn() {
    var name = document.getElementById("idName");
    var fLastName = document.getElementById("idFLastName");
    var sLastName = document.getElementById("idSLastName");
    var email = document.getElementById("idEmail");
    var passwordU = document.getElementById("idPassword");
    var passwordC = document.getElementById("idPasswordC")
    var username = document.getElementById("idUsername");
    var birthday = document.getElementById("idBirthday");
    var cellPhone = document.getElementById("idNumCell");
    if (validateString(name.value) == true){
        if(validateString(fLastName.value) == true){
            if(validateString(sLastName.value) == true){
                if (validateCellphone(cellPhone.value) == true){
                    if (validateEmail(email.value) == true){
                        if (validateString(username.value) == true){
                            if (validatesPassword(passwordU.value) == true){
                                if (validatesPassword(passwordC.value) == true){
                                    if (passwordU.value == passwordC.value){
                                        return verifyUser(name,fLastName,sLastName,email,passwordU,username,birthday,cellPhone);
                                    }
                                    document.getElementById("status").innerHTML = "Passwords are different"
                                    // alert("Passwords are not the same");
                                    return false;
                                }
                                document.getElementById("status").innerHTML = "Incorrect confirmation password"
                                // alert("Incorrect confirmation password");
                                return false;
                            }
                            document.getElementById("status").innerHTML = "Incorrect password format"
                            // alert("Incorrect password format")
                            return false
                        }
                        document.getElementById("status").innerHTML = "Incorrect username"
                        // alert("Incorrect username")
                        return false
                    }
                    document.getElementById("status").innerHTML = "Incorrect email"
                    // alert("Incorrect email")
                    return false
                }
                document.getElementById("status").innerHTML = "Incorrect cell phone"
                // alert("Incorrect cell phone")
                return false
            }
            document.getElementById("status").innerHTML = "Incorrect second last name"
            // alert("Incorrect second last name")
            return false
        }
        document.getElementById("status").innerHTML = "Incorrect first last name"
        // alert("Incorrect first last name")
        return false
    }
    document.getElementById("status").innerHTML = "Incorrect name"
    // alert("Incorrect name")
    return false
}

function verifyUser(name,fLastName,sLastName,email,passwordU,username,birthday,cellPhone) {
    var yes = false;

    for (i = 0; i != userList.length; i++){
        if((userList[i].email == email.value) || (userList[i].username == username.value)){
            yes = true;
        }
    }

    if(yes == true){
        document.getElementById("status").innerHTML = "Email/User is already taken"
        // alert("Email/User is already taken");
    }
    else{
        createUser();
    }
}

//Validates input data

function validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {
        return false;
    }
    return true;
}

function validateString(dataS) {
    console.log(dataS)
    if (dataS == ""){
        return false;
    }
    else{
        return true;
    }
}

function validateCellphone(cell) {
    var reg = /^[0-9]{8}$/
    if (reg.test(cell) == false){
        return false;
    }
    return true;
}

function validatesPassword(password) {
    var reg = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})")
    if (reg.test(password) == false){
        return false;
    }
    else{
        return true;
    }
}