meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
lasemana = ["Domingo", "Lunes", "Martes", "Jueves", "Viernes", "Sabado"];
diassemana = ["lun", "mar", "mie", "jue", "vie", "sab", "dom"];

window.onload = function() {
    //fecha actual
    hoy = new Date();
    diasemhoy = hoy.getDay();
    diahoy = hoy.getDate();
    meshoy = hoy.getMonth();
    annohoy = hoy.getFullYear();

    //Elementos del DOM: en cabecera de calendario
    tit = document.getElementById("titulos");
    ant = document.getElementById("anterior");
    pos = document.getElementById("posterior");

    //Elementos del DOM en primera fila
    f0 = document.getElementById("fila0");

    //Pie de calendario
    pie = document.getElementById("fechaactual");
    pie.innerHTML += lasemana[diasemhoy] + ", " + diahoy + " de " + meses[meshoy] + " de " + annohoy;

    //Formulario: datos iniciales:
    document.buscar.buscaanno.value = annohoy;

    //Definir elementos iniciales:
    mescal = meshoy;
    annocal = annohoy;

    cabecera();
    primeralinea();
    escribirdias();
}

function cabecera() {
    tit.innerHTML = meses[mescal] + " de " + annocal;
    mesant = mescal - 1; //mes anterior
    mespos = mescal + 1; //mes posterior
    if (mesant < 0) { mesant = 11; }
    if (mespos > 11) { mespos = 0; }
    ant.innerHTML = meses[mesant];
    pos.innerHTML = meses[mespos];
}

//primera linea de tabla: dias de la semana
function primeralinea() {
    for (i = 0; i < 7; i++) {
        celda0 = f0.getElementsByTagName("th")[i];
        celda0.innerHTML = diassemana[i];
    }
}

function escribirdias() {
    //Buscar dia de la semana del dia 1 del mes:
    primeromes = new Date(annocal, mescal, "1"); //buscar primer dia del mes
    prsem = primeromes.getDay(); //Buscar dia de la semana del dia 1
    prsem--; //adaptar al calendario español (empezar lunes)
    if (prsem == -1) { prsem = 6; }
    //buscar fecha para primera celda:
    diaprmes = primeromes.getDate();
    prcelda = diaprmes - prsem; //restar dias que sobran de la semana
    empezar = primeromes.setDate(prcelda); //empezar = tiempo UNIX 1 celda
    diames = new Date() // convertir en fecha
    diames.setTime(empezar); //diames = fecha primera celda.
    //Recorrer las celdas para escribir el dia:
    for (i = 1; i < 7; i++) {
        fila = document.getElementById("fila" + i);
        for (j = 0; j < 7; j++) {
            midia = diames.getDate();
            mimes = diames.getMonth();
            mianno = diames.getFullYear();
            celda = fila.getElementsByTagName("td")[j];
            celda.innerHTML = midia;
            // Recuperar estado inicial al cambiar de mes:
            celda.style.backgroundColor = "#c7f0db";
            celda.style.color = "#492736";
            //domingos en rojo
            if (j == 6) {
                celda.style.color = "#f11445";
            }
            //dias restantes del mes en gris
            if (mimes != mescal) {
                celda.style.color = "#a0babc";
            }
            //destacar la fecha actual
            if (mimes == meshoy && midia == diahoy && mianno == annohoy) {
                celda.style.backgroundColor = "#f0b19e";
                celda.innerHTML = "<cite title = 'Fecha Actual'>" + midia + "</cite>";
            }
            //pasar al siguiente dia
            midia = midia + 1;
            diames.setDate(midia);
        }
    }

}

//ver mes anterior
function mesantes() {
    nuevomes = new Date();
    primeromes--;
    nuevomes.setTime(primeromes);
    mescal = nuevomes.getMonth();
    annocal = nuevomes.getFullYear();
    cabecera();
    escribirdias();
}

//ver mes posterior
function mesdespues() {
    nuevomes = new Date();
    tiempounix = primeromes.getTime();
    tiempounix = tiempounix + (45 * 24 * 60 * 60 * 1000); //le añadimos 45 dias
    nuevomes.setTime(tiempounix);
    mescal = nuevomes.getMonth();
    annocal = nuevomes.getFullYear();
    cabecera();
    escribirdias();
}

//volver al mes actual
function actualizar() {
    mescal = hoy.getMonth();
    annocal = hoy.getFullYear();
    cabecera();
    escribirdias();
}

function mifecha() {
    mianno = document.buscar.buscaanno.value;
    listameses = document.buscar.buscames;
    opciones = listameses.options;
    num = listameses.selectedIndex;
    mimes = opciones[num].value;

    if (isNaN(mianno) || mianno < 1) {
        alert("El año no es valido: \n debe ser un numero mayor que 0");
    } else {
        mife = new Date();
        mife.setMonth(mimes);
        mife.setFullYear(mianno);
        mescal = mife.getMonth();
        annocal = mife.getFullYear();
        cabecera();
        escribirdias();
    }
}